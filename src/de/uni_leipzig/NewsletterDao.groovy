package de.uni_leipzig

import groovy.sql.Sql

class NewsletterDao {

	private static insertStmtString = "INSERT INTO emails(email) VALUES (:email)";
	private static selectStmtString = "SELECT * FROM emails WHERE id=:id";
	private static selectAllStmtString = "SELECT * FROM emails";
	private static updateStmtString = "UPDATE emails SET email=:email WHERE id=:id";
	private static deleteStmtString = "DELETE FROM emails WHERE id=:id";
	
	private Sql sql = Sql.newInstance("jdbc:mysql://localhost:3306/webtech2", "root", "", "com.mysql.jdbc.Driver")

	Newsletter create(mail){
		def r = sql.executeInsert(insertStmtString, [email:mail.email])
		new Newsletter(id: r[0][0], email : mail.email)
	}
	
	Newsletter update(mail){
		def r = sql.executeUpdate(updateStmtString, [id : mail.id, email:mail.email])
		get(mail.id)
	}
	
	
	Newsletter get(id){
		def r = sql.firstRow(selectStmtString, [id:id])
		if(r) new Newsletter(id: r.id, email : r.email)
	}
	
	void delete(id){
		sql.execute(deleteStmtString, [id:id])
	}
	
	def test(){
		sql.eachRow("SELECT * FROM emails")
		{
			println "id: ${it.id} email: {it.email}"
		}
	}
	
	
	static def main(args){
		println(new NewsletterDao().update(new Newsletter(id : 3, email : "f@a.org")))
	}
}
