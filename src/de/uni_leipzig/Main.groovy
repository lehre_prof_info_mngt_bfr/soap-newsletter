package de.uni_leipzig

import javax.jws.WebMethod
import javax.jws.WebParam
import javax.jws.WebResult
import javax.jws.WebService
import javax.jws.soap.SOAPBinding
import javax.swing.JOptionPane
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.ws.Endpoint

class Main {
	static def main(args){
		Endpoint endpoint = Endpoint.publish( "http://localhost:8080/services", new NewsletterService() );
		JOptionPane.showMessageDialog( null, "Server beenden" );
		endpoint.stop();
	}
}
