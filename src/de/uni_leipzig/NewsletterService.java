
package de.uni_leipzig;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlElement;

@WebService(name = "NewsletterWebService",  serviceName = "NewsletterWebService", portName = "NewsletterWebServicePort", targetNamespace = "http://uni-leipzig.de/ws/namespace")
@SOAPBinding(style=SOAPBinding.Style.DOCUMENT, use=SOAPBinding.Use.ENCODED, parameterStyle=SOAPBinding.ParameterStyle.WRAPPED)
public class NewsletterService {
	
	private NewsletterDao dao = new NewsletterDao();
	
	@WebMethod
	public Newsletter create(@WebParam(name = "newsletter") 
	 					 @XmlElement(required=true, nillable=false)	
						 Newsletter b){
		return dao.create(b);
	}
	
	@WebMethod
	public Newsletter get(@WebParam(name = "id") 
	 					 @XmlElement(required=true, nillable=false)	
						 int id){
		return dao.get(id);
	}
	
	@WebMethod
	public Newsletter update(@WebParam(name = "newsletter") 
	 					 @XmlElement(required=true, nillable=false)	
						 Newsletter b){
		return dao.update(b);
	}
	
	@WebMethod
	public void delete(@WebParam(name = "id") 
	 					 @XmlElement(required=true, nillable=false)	
						 int id){
		dao.delete(id);
	}
	
}